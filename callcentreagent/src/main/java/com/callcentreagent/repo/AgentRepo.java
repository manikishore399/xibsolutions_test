package com.callcentreagent.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.callcentreagent.model.Agent;

public interface AgentRepo extends JpaRepository<Agent, Long> {

}
