package com.callcentreagent.controllerimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.callcentreagent.controller.ManagerController;
import com.callcentreagent.model.Manager;
import com.callcentreagent.service.ManagerService;

@RestController
public class ManagerControllerImpl implements ManagerController {
	@Autowired
	private ManagerService managerService;

	// Creates a new manager with the specified details
	@Override
	@PostMapping(value = "/manager")
	public Manager addNewManager(@RequestBody Manager manager) {
		return managerService.addManager(manager);
	}
}
