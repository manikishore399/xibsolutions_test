package com.callcentreagent.service;

import java.util.List;

import com.callcentreagent.model.Manager;

public interface ManagerService {

	public Manager addManager(Manager manager);

	public List<Manager> getAllManagers(Manager manager);

}
