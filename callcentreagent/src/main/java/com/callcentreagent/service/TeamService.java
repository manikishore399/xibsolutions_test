package com.callcentreagent.service;

import java.util.List;

import com.callcentreagent.model.Team;

public interface TeamService {
	public Team saveTeam(Team team);

	public Team getTeamById(Long id);

	public List<Team> getAllTeams();
}
