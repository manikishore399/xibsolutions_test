package com.callcentreagent.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {
	@org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
	public ResponseEntity<Object> handleAllExceptions(Exception exception, WebRequest request) {
		String errorMessage = exception.getLocalizedMessage();
		if (errorMessage == null) {
			errorMessage = exception.toString();
		}
		return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = NullPointerException.class)
	public ResponseEntity<Object> handleNullException(NullPointerException exception, WebRequest request) {
		String errorMessage = exception.getLocalizedMessage();
		if (errorMessage == null) {
			errorMessage = exception.toString();
		}
		return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.NO_CONTENT);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = UserServiceException.class)
	public ResponseEntity<Object> handleUserServiceException(UserServiceException exception, WebRequest request) {
		String errorMessage = exception.getLocalizedMessage();
		if (errorMessage == null) {
			errorMessage = exception.toString();
		}
		return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
