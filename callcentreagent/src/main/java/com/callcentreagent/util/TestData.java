package com.callcentreagent.util;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.callcentreagent.model.Agent;
import com.callcentreagent.model.Manager;
import com.callcentreagent.model.Team;
import com.callcentreagent.repo.AgentRepo;
import com.callcentreagent.repo.ManagerRepo;
import com.callcentreagent.service.TeamService;

@Component
public class TestData {

	@Autowired
	private AgentRepo agentRepo;
	@Autowired
	private ManagerRepo managerRepo;
	@Autowired
	private TeamService teamService;

	@PostConstruct
	@Transactional
	public void execute() {
		// manager input
		Manager manager1 = saveManager(new Manager(101l, "mani", "kishore"));
		Manager manager2 = saveManager(new Manager(103l, "mahesh", "nanda"));
		// team input
		Team team1 = saveTeam(new Team(201l, "TEAM1", manager1));
		Team team2 = saveTeam(new Team(202l, "TEAM2", manager2));
		// agent input
		saveAgent(new Agent(301l, "AGENT1", "AGENTLASTNAME1", manager1, team1));
		saveAgent(new Agent(302l, "AGENT2", "AGENTLASTNAME2", manager1, team1));
		saveAgent(new Agent(303l, "AGENT3", "AGENTLASTNAME3", manager2, team2));
		saveAgent(new Agent(304l, "AGENT4", "AGENTLASTNAME4", manager2, team2));

	}

	private Manager saveManager(Manager manager) {
		return managerRepo.save(manager);
	}

	private Team saveTeam(Team team) {
		return teamService.saveTeam(team);
	}

	private Agent saveAgent(Agent agent) {
		return agentRepo.save(agent);
	}
}
