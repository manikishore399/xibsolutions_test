package com.callcentreagent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CallcentreagentApplication {

	public static void main(String[] args) {
		SpringApplication.run(CallcentreagentApplication.class, args);
	}

}
